package net.thegaminghuskymc.huskylib2.lib.items

import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.Item
import net.minecraft.util.ResourceLocation
import net.thegaminghuskymc.huskylib2.lib.utils.ProxyRegistry

open class ItemBase(name: String, modID: String, creativetab: CreativeTabs) : Item() {

    init {
        unlocalizedName = name
        creativeTab = creativetab
        setUnlocalizedName(name, modID)
    }

    fun setUnlocalizedName(name: String, modID: String): Item {
        unlocalizedName = name
        registryName = ResourceLocation(modID, name)
        ProxyRegistry.register<Item>(this)
        return this
    }

}
