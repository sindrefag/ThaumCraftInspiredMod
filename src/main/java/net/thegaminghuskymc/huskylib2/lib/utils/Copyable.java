package net.thegaminghuskymc.huskylib2.lib.utils;

public interface Copyable<T> {

    T copy();
}
