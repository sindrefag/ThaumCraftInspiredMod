package net.thegaminghuskymc.huskylib2.lib.blocks;

import net.timmy.th2.Reference;

public interface IModBlock extends net.thegaminghuskymc.huskylib2.lib.interf.IModBlock {

    @Override
    default String getModNamespace() {
        return Reference.MODID;
    }

}
