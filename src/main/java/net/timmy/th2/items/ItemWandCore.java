package net.timmy.th2.items;

import net.thegaminghuskymc.huskylib2.lib.items.ItemMod;
import net.timmy.th2.Reference;

public class ItemWandCore extends ItemMod {

    public ItemWandCore(String name) {
        super("wand_core_" + name);
    }

    @Override
    public String getModNamespace() {
        return Reference.MODID;
    }

}
