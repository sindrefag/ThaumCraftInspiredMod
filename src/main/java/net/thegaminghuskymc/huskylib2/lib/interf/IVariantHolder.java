package net.thegaminghuskymc.huskylib2.lib.interf;

import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import static net.timmy.th2.Reference.MODID;

public interface IVariantHolder {

    String[] getVariants();

    @SideOnly(Side.CLIENT)
    ItemMeshDefinition getCustomMeshDefinition();

    default String getUniqueModel() {
        return null;
    }

    String getModNamespace();

    default String getPrefix() {
        return MODID;
    }
}
