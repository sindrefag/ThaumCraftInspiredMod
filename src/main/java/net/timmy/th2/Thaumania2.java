package net.timmy.th2;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Biomes;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.terraingen.DecorateBiomeEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.thegaminghuskymc.huskylib2.lib.utils.ModelHandler;
import net.timmy.th2.blocks.Sapling;
import net.timmy.th2.init.Th2Blocks;
import net.timmy.th2.init.Th2Entities;
import net.timmy.th2.init.Th2Items;
import net.timmy.th2.proxy.CommonProxy;

import java.util.Random;
import java.util.logging.Logger;

@Mod(modid = Reference.MODID, name = Reference.NAME, version = Reference.VERSION)
public class Thaumania2 {

    @Mod.Instance
    public static Thaumania2 instance;

    public static Logger LOGGER = Logger.getLogger(Reference.NAME);

    @SidedProxy(clientSide = Reference.CSIDE, serverSide = Reference.SSIDE)
    public static CommonProxy proxy;

    public static CreativeTabs th2Blocks = new Th2Tab(TextFormatting.GOLD + "Thaumania 2: Blocks");
    public static CreativeTabs th2Items = new Th2Tab(TextFormatting.GOLD + "Thaumania 2: Items");
    public static CreativeTabs th2Wands = new Th2Tab(TextFormatting.GOLD + "Thaumania 2: Wands");

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        Th2Blocks.register();
        Th2Items.register();
        proxy.preInit(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }

    @SubscribeEvent
    public void decorate(DecorateBiomeEvent.Decorate event) {

        double grandOakPerChunk = 1;

        World world = event.getWorld();
        Biome biome = world.getBiome(event.getPos());
        Random rand = event.getRand();
        Sapling grandOakSapling = new Sapling();

        if (
                (biome == Biomes.TAIGA ||
                        biome == Biomes.TAIGA_HILLS ||
                        biome == Biomes.COLD_TAIGA ||
                        biome == Biomes.COLD_TAIGA_HILLS ||
                        biome == Biomes.MUTATED_REDWOOD_TAIGA ||
                        biome == Biomes.MUTATED_REDWOOD_TAIGA_HILLS ||
                        biome == Biomes.MUTATED_TAIGA ||
                        biome == Biomes.MUTATED_TAIGA_COLD ||
                        biome == Biomes.REDWOOD_TAIGA ||
                        biome == Biomes.REDWOOD_TAIGA_HILLS ||
                        biome == Biomes.PLAINS ||
                        biome == Biomes.FOREST ||
                        biome == Biomes.MUTATED_PLAINS ||
                        biome == Biomes.MUTATED_FOREST ||
                        biome == Biomes.FOREST_HILLS ||
                        biome == Biomes.ICE_PLAINS) &&
                        event.getType() == DecorateBiomeEvent.Decorate.EventType.TREE
                ) {
            if (grandOakPerChunk < 1 && rand.nextDouble() > grandOakPerChunk)
                return;

            int amount = (int) Math.max(1, grandOakPerChunk);
            for (int i = 0; i < amount; i++) {
                grandOakSapling.generateSingleTree(false).generate(world, rand, event.getPos());
            }

            Loader.instance().setActiveModContainer(null);
            return;
        }
        return;
    }

}
