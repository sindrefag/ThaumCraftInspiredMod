package net.timmy.th2.items;

import net.thegaminghuskymc.huskylib2.lib.items.ItemMod;
import net.timmy.th2.Reference;

public class ItemWand extends ItemMod {

    public ItemWand(String name) {
        super("wand_" + name);
    }

    @Override
    public String getModNamespace() {
        return Reference.MODID;
    }

}
