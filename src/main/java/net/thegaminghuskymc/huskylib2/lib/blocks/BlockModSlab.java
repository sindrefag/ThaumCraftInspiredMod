package net.thegaminghuskymc.huskylib2.lib.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.thegaminghuskymc.huskylib2.lib.interf.IModBlock;
import net.thegaminghuskymc.huskylib2.lib.items.blocks.ItemModBlockSlab;
import net.thegaminghuskymc.huskylib2.lib.recipie.RecipeHandler;
import net.thegaminghuskymc.huskylib2.lib.utils.ProxyRegistry;

import java.util.HashMap;
import java.util.Random;

public abstract class BlockModSlab extends BlockSlab implements IModBlock {

    private static boolean tempDoubleSlab;
    private static HashMap<BlockModSlab, BlockModSlab> halfSlabs = new HashMap<>();
    private static HashMap<BlockModSlab, BlockModSlab> fullSlabs = new HashMap<>();
    private final String[] variants;
    private boolean doubleSlab;
    private String bareName;

    public BlockModSlab(String name, Material materialIn, boolean doubleSlab) {
        super(hacky(materialIn, doubleSlab));
        this.doubleSlab = doubleSlab;
        if (doubleSlab) {
            name = name + "_double";
        }

        this.bareName = name;
        this.variants = new String[]{name};
        this.setUnlocalizedName(name);
        if (!doubleSlab) {
            this.useNeighborBrightness = true;
            this.setDefaultState(this.blockState.getBaseState().withProperty(HALF, EnumBlockHalf.BOTTOM));
        }

        this.setCreativeTab(doubleSlab ? null : CreativeTabs.BUILDING_BLOCKS);
    }

    private static Material hacky(Material m, boolean doubleSlab) {
        tempDoubleSlab = doubleSlab;
        return m;
    }

    public static void initSlab(Block base, int meta, BlockModSlab half, BlockModSlab full) {
        fullSlabs.put(half, full);
        fullSlabs.put(full, full);
        halfSlabs.put(half, half);
        halfSlabs.put(full, half);
        half.register();
        full.register();
        RecipeHandler.addOreDictRecipe(ProxyRegistry.newStack(half, 6), "BBB", 'B', ProxyRegistry.newStack(base, 1, meta));
    }

    public BlockStateContainer createBlockState() {
        return tempDoubleSlab ? new BlockStateContainer(this, this.getVariantProp()) : new BlockStateContainer(this, HALF, this.getVariantProp());
    }

    public IBlockState getStateFromMeta(int meta) {
        return this.doubleSlab ? this.getDefaultState() : this.getDefaultState().withProperty(HALF, meta == 8 ? EnumBlockHalf.TOP : EnumBlockHalf.BOTTOM);
    }

    public int getMetaFromState(IBlockState state) {
        if (this.doubleSlab) {
            return 0;
        } else {
            return state.getValue(HALF) == EnumBlockHalf.TOP ? 8 : 0;
        }
    }

    @Override
    public String getBareName() {
        return bareName;
    }

    public BlockSlab getFullBlock() {
        return fullSlabs.get(this);
    }

    public BlockSlab getSingleBlock() {
        return halfSlabs.get(this);
    }

    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(this.getSingleBlock());
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isNormalCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean canRenderInLayer(IBlockState state, BlockRenderLayer layer) {
        return layer == BlockRenderLayer.TRANSLUCENT || layer == BlockRenderLayer.CUTOUT || layer == BlockRenderLayer.CUTOUT_MIPPED || layer == BlockRenderLayer.SOLID;
    }

    public Item getItemDropped(IBlockState p_149650_1_, Random p_149650_2_, int p_149650_3_) {
        return Item.getItemFromBlock(this.getSingleBlock());
    }

    public int quantityDropped(IBlockState state, int fortune, Random random) {
        return super.quantityDropped(state, fortune, random);
    }

    public void register() {
        this.setRegistryName(getPrefix(), bareName);
        ProxyRegistry.register(this);
        if (!this.isDouble()) {
            ProxyRegistry.register(new ItemModBlockSlab(this, new ResourceLocation(this.getPrefix(), bareName)));
        }

    }

    public String[] getVariants() {
        return this.variants;
    }

    public ItemMeshDefinition getCustomMeshDefinition() {
        return null;
    }

    public EnumRarity getBlockRarity(ItemStack stack) {
        return EnumRarity.COMMON;
    }

    public IProperty[] getIgnoredProperties() {
        return this.doubleSlab ? new IProperty[]{HALF} : new IProperty[]{};
    }

    public String getUnlocalizedName(int meta) {
        return this.getUnlocalizedName();
    }

    public boolean isDouble() {
        return this.doubleSlab;
    }

    public boolean isFullBlock(IBlockState state) {
        return this.isDouble();
    }

    public boolean isSideSolid(IBlockState base_state, IBlockAccess world, BlockPos pos, EnumFacing side) {
        IBlockState state = this.getActualState(base_state, world, pos);
        return this.isDouble() || state.getValue(BlockSlab.HALF) == EnumBlockHalf.TOP && side == EnumFacing.UP || state.getValue(BlockSlab.HALF) == EnumBlockHalf.BOTTOM && side == EnumFacing.DOWN;
    }

    public IProperty<?> getVariantProp() {
        return HALF;
    }

    public IProperty<?> getVariantProperty() {
        return HALF;
    }

    public Class getVariantEnum() {
        return EnumBlockHalf.class;
    }

    public Comparable<?> getTypeForItem(ItemStack stack) {
        return EnumBlockHalf.BOTTOM;
    }

}