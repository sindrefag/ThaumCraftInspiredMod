package net.thegaminghuskymc.huskylib2.lib.utils;

public class Refs {

    public static final String MODID = "hl2";
    public static final String NAME = "HuskyLib2";
    public static final String VERSION = "0.0.1";
    public static final String ACC_MC = "[1.11.2, 1.12.2]";

}
