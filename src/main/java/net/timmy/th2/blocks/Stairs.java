package net.timmy.th2.blocks;

import net.minecraft.block.Block;
import net.timmy.th2.blocks.base.ModStairs;

public class Stairs extends ModStairs {

    public Stairs(String name, Block base) {
        super(name, base.getDefaultState());
    }

}
