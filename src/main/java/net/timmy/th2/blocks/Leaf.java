package net.timmy.th2.blocks;

import net.minecraft.block.Block;
import net.timmy.th2.blocks.base.ModLeaf;

public class Leaf extends ModLeaf {

    public Leaf(String name, Block sapling) {
        super(name, sapling);
    }

}