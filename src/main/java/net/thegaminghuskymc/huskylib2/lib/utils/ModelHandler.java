package net.thegaminghuskymc.huskylib2.lib.utils;


import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.IStateMapper;
import net.minecraft.client.renderer.block.statemap.StateMap.Builder;
import net.minecraft.client.renderer.color.BlockColors;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.thegaminghuskymc.huskylib2.lib.blocks.IModBlock;
import net.thegaminghuskymc.huskylib2.lib.interf.IBlockColorProvider;
import net.thegaminghuskymc.huskylib2.lib.interf.IExtraVariantHolder;
import net.thegaminghuskymc.huskylib2.lib.interf.IItemColorProvider;
import net.thegaminghuskymc.huskylib2.lib.interf.IVariantHolder;
import net.thegaminghuskymc.huskylib2.lib.items.ItemMod;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;

public final class ModelHandler {

    public static final HashMap<String, ModelResourceLocation> resourceLocations = new HashMap<>();

    public ModelHandler() {
    }

    @SubscribeEvent
    public static void onRegister(ModelRegistryEvent event) {

        for (Object variantHolder : ItemMod.variantHolders) {
            IVariantHolder holder = (IVariantHolder) variantHolder;
            registerModels(holder);
        }

    }

    private static void registerModels(IVariantHolder holder) {
        String unique = holder.getUniqueModel();
        String prefix = holder.getPrefix();
        Item i = (Item) holder;
        ItemMeshDefinition def = holder.getCustomMeshDefinition();
        if (def != null) {
            ModelLoader.setCustomMeshDefinition((Item) holder, def);
        } else {
            registerModels(i, prefix, holder.getVariants(), unique, false);
        }

        if (holder instanceof IExtraVariantHolder) {
            IExtraVariantHolder extra = (IExtraVariantHolder) holder;
            registerModels(i, prefix, extra.getExtraVariants(), unique, true);
        }

    }

    private static void registerModels(Item item, String prefix, String[] variants, String uniqueVariant, boolean extra) {
        if (item instanceof ItemBlock && ((ItemBlock) item).getBlock() instanceof IModBlock) {
            IModBlock hl2Block = (IModBlock) ((ItemBlock) item).getBlock();
            Class clazz = hl2Block.getVariantEnum();
            IProperty variantProp = hl2Block.getVariantProp();
            boolean ignoresVariant = false;
            IStateMapper mapper = hl2Block.getStateMapper();
            IProperty[] ignored = hl2Block.getIgnoredProperties();
            if (mapper != null || ignored != null && ignored.length > 0) {
                if (mapper != null) {
                    ModelLoader.setCustomStateMapper((Block) hl2Block, mapper);
                } else {
                    Builder builder = new Builder();

                    for (IProperty p : ignored) {
                        if (p == variantProp) {
                            ignoresVariant = true;
                        }

                        builder.ignore(p);
                    }

                    ModelLoader.setCustomStateMapper((Block) hl2Block, builder.build());
                }
            }

            if (clazz != null && !ignoresVariant) {
                registerVariantsDefaulted(item, (Block) hl2Block, clazz, variantProp.getName());
                return;
            }
        }

        for (int i = 0; i < variants.length; ++i) {
            String var = variants[i];
            if (!extra && uniqueVariant != null) {
                var = uniqueVariant;
            }

            ModelResourceLocation loc = new ModelResourceLocation(new ResourceLocation(prefix, var), "inventory");
            if (!extra) {
                ModelLoader.setCustomModelResourceLocation(item, i, loc);
                resourceLocations.put(getKey(item, i), loc);
            } else {
                ModelBakery.registerItemVariants(item, loc);
                resourceLocations.put(variants[i], loc);
            }
        }

    }

    private static <T extends Enum<T> & IStringSerializable> void registerVariantsDefaulted(Item item, Block b, Class<T> enumclazz, String variantHeader) {
        String baseName = Objects.requireNonNull(b.getRegistryName()).toString();
        Enum[] var5 = enumclazz.getEnumConstants();

        for(Enum aVar5 : var5) {
            T e = (T) aVar5;
            String variantName = variantHeader + "=" + (e).getName();
            ModelResourceLocation loc = new ModelResourceLocation(baseName, variantName);
            int i = e.ordinal();
            ModelLoader.setCustomModelResourceLocation(item, i, loc);
            resourceLocations.put(getKey(item, i), loc);
        }

    }

    private static String getKey(Item item, int meta) {
        return "i_" + item.getRegistryName() + "@" + meta;
    }

    public static void init() {
        ItemColors itemColors = Minecraft.getMinecraft().getItemColors();
        BlockColors blockColors = Minecraft.getMinecraft().getBlockColors();

        for (Object variantHolder : ItemMod.variantHolders) {
            IVariantHolder holder = (IVariantHolder) variantHolder;
            if (holder instanceof IItemColorProvider) {
                itemColors.registerItemColorHandler(((IItemColorProvider) holder).getItemColor(), (Item) holder);
            }

            if (holder instanceof ItemBlock && ((ItemBlock) holder).getBlock() instanceof IBlockColorProvider) {
                Block block = ((ItemBlock) holder).getBlock();
                blockColors.registerBlockColorHandler(((IBlockColorProvider) block).getBlockColor(), block);
                itemColors.registerItemColorHandler(((IBlockColorProvider) block).getItemColor(), block);
            }
        }

    }

}