package net.timmy.th2.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockColored;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.util.ResourceLocation;
import net.thegaminghuskymc.huskylib2.lib.blocks.BlockMod;
import net.thegaminghuskymc.huskylib2.lib.utils.ProxyRegistry;
import net.timmy.th2.Reference;
import net.timmy.th2.Thaumania2;
import net.timmy.th2.items.ItemBlockCandle;

public class BlockCandle extends BlockMod {

    public BlockCandle(Material material, String name) {
        super(name, material, name);
        setCreativeTab(Thaumania2.th2Blocks);
    }

    @Override
    protected BlockStateContainer createBlockState() {
    	return new BlockStateContainer(this, BlockColored.COLOR);
    }
    
    @Override
    public IBlockState getStateFromMeta(int meta) {
    	return this.getDefaultState().withProperty(BlockColored.COLOR, EnumDyeColor.byMetadata(meta));
    }
    
    @Override
    public int getMetaFromState(IBlockState state) {
    	return state.getValue(BlockColored.COLOR).getMetadata();
    }
    
    @Override
    public String getModNamespace() {
        return Reference.MODID;
    }

    public Block setUnlocalizedName(String name) {
        super.setUnlocalizedName(name);
        ProxyRegistry.register(new ItemBlockCandle(this, new ResourceLocation(this.getPrefix(), name)));
        return this;
    }

}