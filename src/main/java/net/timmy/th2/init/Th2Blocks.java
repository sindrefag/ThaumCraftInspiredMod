package net.timmy.th2.init;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.thegaminghuskymc.huskylib2.lib.blocks.BlockModSlab;
import net.thegaminghuskymc.huskylib2.lib.blocks.BlockModStairs;
import net.timmy.th2.Thaumania2;
import net.timmy.th2.blocks.*;
import net.timmy.th2.blocks.base.ModStairs;
import net.timmy.th2.properties.*;

import java.util.LinkedList;
import java.util.List;

public class Th2Blocks {

    public static List<net.minecraft.block.Block> defaultItemBlocksToRegister = new LinkedList<>();
    public static List<net.minecraft.block.Block> customNameItemBlocksToRegister = new LinkedList<>();
    public static List<IBlockColored> pendingIBlockColorBlocks = new LinkedList<>();

    private static net.minecraft.block.Block altar, awakenedSiderichor, pedestal, rechargePedestal, siderichor, stoneTable, woodTable, researchTable, cauldron;

    public static net.minecraft.block.Block candles;

    private static net.minecraft.block.Block rune, slab, stair;

    private static net.minecraft.block.Block log, leaf, sapling, plank, bark;

    static {
        altar = new Block(Material.IRON, "altar").setCreativeTab(Thaumania2.th2Blocks);
        queueDefaultItemBlock(altar);
        awakenedSiderichor = new Block(Material.IRON, "awakened_siderichor").setCreativeTab(Thaumania2.th2Blocks);
        queueDefaultItemBlock(awakenedSiderichor);
        pedestal = new Block(Material.IRON, "pedestal").setCreativeTab(Thaumania2.th2Blocks);
        queueDefaultItemBlock(pedestal);
        rechargePedestal = new Block(Material.IRON, "recharge_pedestal").setCreativeTab(Thaumania2.th2Blocks);
        queueDefaultItemBlock(rechargePedestal);
        siderichor = new Block(Material.IRON, "siderichor").setCreativeTab(Thaumania2.th2Blocks);
        queueDefaultItemBlock(siderichor);
        stoneTable = new Block(Material.ROCK, "stone_table").setCreativeTab(Thaumania2.th2Blocks);
        queueDefaultItemBlock(stoneTable);
        woodTable = new Block(Material.WOOD, "wood_table").setCreativeTab(Thaumania2.th2Blocks);
        queueDefaultItemBlock(woodTable);
        researchTable = new Block(Material.WOOD, "research_table").setCreativeTab(Thaumania2.th2Blocks);
        queueDefaultItemBlock(researchTable);
        cauldron = new Block(Material.IRON, "cauldron").setCreativeTab(Thaumania2.th2Blocks);
        queueDefaultItemBlock(cauldron);

        for (EnumRuneType type : EnumRuneType.values()) {
            rune = new Rune1("runes_" + type.getName()).setCreativeTab(Thaumania2.th2Blocks);

            slab = new Slab(type.getName() + "_slab", false).setCreativeTab(Thaumania2.th2Blocks);
            BlockModSlab.initSlab(rune, type.getMeta(), (BlockModSlab) slab, new Slab(type.getName() + "_slab",true));

            /*stair = new Stairs(type.getName() + "_stair", rune).setCreativeTab(Thaumania2.th2Blocks);
            BlockModStairs.initStairs(rune, type.getMeta(), (BlockModStairs) stair);*/

        }

        for (EnumRuneType2 type : EnumRuneType2.values()) {
            rune = new Rune2("runes_" + type.getName()).setCreativeTab(Thaumania2.th2Blocks);

            slab = new Slab(type.getName() + "_slab",false).setCreativeTab(Thaumania2.th2Blocks);
            BlockModSlab.initSlab(rune, type.getMeta(), (BlockModSlab) slab, new Slab(type.getName() + "_slab",true));

            /*stair = new Stairs(type.getName() + "_stair",rune).setCreativeTab(Thaumania2.th2Blocks);
            BlockModStairs.initStairs(rune, type.getMeta(), (BlockModStairs) stair);*/

        }

        for (EnumWoodLogType type : EnumWoodLogType.values()) {
            log = new Log(type.getName() + "_log").setCreativeTab(Thaumania2.th2Blocks);

            bark = new Block(Material.WOOD, type.getName() + "_bark").setCreativeTab(Thaumania2.th2Blocks);
        }

        for (EnumWoodType type : EnumWoodType.values()) {
            plank = new Block(Material.WOOD, type.getName() + "_plank").setCreativeTab(Thaumania2.th2Blocks);

            slab = new Slab(type.getName() + "_slab", false).setCreativeTab(Thaumania2.th2Blocks);
            BlockModSlab.initSlab(plank, type.getMeta(), (BlockModSlab) slab, new Slab(type.getName() + "_slab", true));

            stair = new Stairs(type.getName() + "_stair", plank).setCreativeTab(Thaumania2.th2Blocks);
            BlockModStairs.initStairs(plank, type.getMeta(), (ModStairs) stair);
        }

        for (EnumWoodLeavesType type : EnumWoodLeavesType.values()) {
            leaf = new Leaf(type.getName() + "_leaf", sapling).setCreativeTab(Thaumania2.th2Blocks);
        }

      	candles = new BlockCandle(Material.CLOTH, "candle");
        registerBlock(candles);
    }

    public static void queueDefaultItemBlock(net.minecraft.block.Block block) {
        defaultItemBlocksToRegister.add(block);
    }

    private static <T extends net.minecraft.block.Block> T registerBlock(T block) {
        if(block instanceof IBlockColored) {
            pendingIBlockColorBlocks.add((IBlockColored) block);
        }
        return block;
    }

    public static void queueCustomNameItemBlock(net.minecraft.block.Block block) {
        customNameItemBlocksToRegister.add(block);
    }

    public static void register() {

    }
}
