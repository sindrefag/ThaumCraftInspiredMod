package net.thegaminghuskymc.huskylib2.lib.utils;

public enum ElementAlignment {

    LEFT,
    RIGHT,
    TOP,
    BOTTOM,
    NONE

}
