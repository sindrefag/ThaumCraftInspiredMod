package net.timmy.th2.items;

import net.minecraft.item.ItemStack;

public interface IItemColored {

    public int colorMultiplier(ItemStack stack, int tintIndex);

}
